﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MTracker.DataContracts;

namespace MTracker.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private MTackerDbContext _dbContext;

        public UnitOfWork(MTackerDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public int Commit()
        {
            return this._dbContext.SaveChanges(false);
        }

        public void Dispose()
        {
            if (this._dbContext != null)
            {
                this._dbContext.Dispose();
                this._dbContext = null;
            }
        }
    }
}
