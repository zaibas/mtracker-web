﻿using MTracker.DataContracts;
using MTracker.DataEntities;

namespace MTracker.DataAccess
{
    public class DeviceMeasurementRepo: GenericRepository<DeviceMeasurement>, IDeviceMeasurementRepo
    {
        public DeviceMeasurementRepo(MTackerDbContext dbContext) : base(dbContext)
        {
        }
    }
}