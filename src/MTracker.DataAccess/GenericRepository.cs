﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Entity;
using MTracker.DataContracts;
using MTracker.DataEntities;

namespace MTracker.DataAccess
{
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IEntity
    {
        protected MTackerDbContext DbContext;
        protected readonly DbSet<TEntity> DbSet;

        protected GenericRepository(MTackerDbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = dbContext.Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return DbSet;
        } 

        public TEntity GetById(string id)
        {
            return DbSet.SingleOrDefault(a => a.Id == id);
        }

        public TEntity Add(TEntity entity)
        {
            return DbSet.Add(entity).Entity;
        }

        public TEntity Delete(TEntity entity)
        {
            return DbSet.Remove(entity).Entity;
        }

        public void Edit(TEntity entity)
        {
            DbSet.Update(entity);
        }
    }
}
