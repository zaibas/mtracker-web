﻿using MTracker.DataContracts;
using MTracker.DataEntities;

namespace MTracker.DataAccess
{
    public class DeviceRepo: GenericRepository<Device>, IDeviceRepo
    {
        public string test { get; set; }
        public DeviceRepo(MTackerDbContext dbContext) : base(dbContext)
        {
        }
    }
}