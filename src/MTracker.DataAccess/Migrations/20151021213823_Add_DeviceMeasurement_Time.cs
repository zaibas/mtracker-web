using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace MTracker.DataAccess.Migrations
{
    public partial class Add_DeviceMeasurement_Time : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "DeviceId",
                table: "DeviceMeasurement",
                nullable: false);
            migrationBuilder.AddColumn<DateTime>(
                name: "Time",
                table: "DeviceMeasurement",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "Time", table: "DeviceMeasurement");
            migrationBuilder.AlterColumn<string>(
                name: "DeviceId",
                table: "DeviceMeasurement",
                nullable: true);
        }
    }
}
