﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using MTracker.DataEntities;

namespace MTracker.DataAccess
{
    public class MTackerDbContext : IdentityDbContext<ApplicationUser>
    {
        public MTackerDbContext()
        {
            //Database.EnsureCreated();
            Database.Migrate();
        }

        public DbSet<Device> Devices { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Device>(b =>
            {
                b.HasKey(d => d.Id);
                b.HasOne(d => d.User).WithMany(u => u.Devices).Required();
                b.HasMany(d => d.Measurements).WithOne().ForeignKey(dm => dm.DeviceId);
                b.ToTable("Device");
            });
            builder.Entity<DeviceMeasurement>(b =>
            {
                b.HasKey(dm => dm.Id);
                b.Property(dm => dm.Measurement).IsRequired();
                b.Property(dm => dm.Time).IsRequired();
                b.HasOne(dm => dm.Device).WithMany(d => d.Measurements).ForeignKey(dm => dm.DeviceId).Required();
                b.ToTable("DeviceMeasurement");
            });
        }
    }
}
