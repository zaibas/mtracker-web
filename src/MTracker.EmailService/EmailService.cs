﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MTracker.EmailService
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class EmailAsyncService
    {
        public static async Task SendEmailAsync(string email, string subject, string message)
        {
            MailMessage msg = new MailMessage("mtracker.measurement@gmail.com", email)
            {
                Body = message + "\r\n",
                BodyEncoding = System.Text.Encoding.UTF8,
                Subject = subject,
                SubjectEncoding = System.Text.Encoding.UTF8,
                IsBodyHtml = true
            };

            using (var client = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "mtracker.measurement@gmail.com",
                    Password = "mtracker12@"
                };
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                client.Credentials = credential;
                await client.SendMailAsync(msg);
            }
        }
    }
}
