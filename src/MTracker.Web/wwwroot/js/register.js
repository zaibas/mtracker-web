﻿
checkSimpleInput(3);
checkPassword();
checkPasswordConfirm();
checkEmailInput();
checkUserNameInput();

function checkUserNameInput() {
    var name = $('#UserName');

    name.keydown(function () {
        if (name.val().length >= 5) {
            showInputLoader(name);
        }
        else {
            hideInputLoader(name);
            showUniqueInputFail(name);
        }
    });

    name.focusout(function () {
        if (isCorrectUsername(name.val())) {
            var url = "/register/checkName";
            var nameValue = name.val();
            $.ajax({
                async: true,
                url: url,
                data: { name: nameValue },
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    if (data["status"] == "ok") {
                        showUniqueInputSuccess(name);
                    }
                    else {
                        showUniqueInputFail(name);
                    }
                    hideInputLoader(name);
                }
            });
        }
        else {
            hideInputLoader(name);
            showUniqueInputFail(name);
        }
    });
}

function checkSimpleInput(stringLegth) {
    $('input.simpleCheck').keydown(function (e) {
        var target = $(e.target);
        if (target.val().length >= stringLegth) {
            showUniqueInputSuccess(target);
        }
        else {
            showUniqueInputFail(target);
        }
    });
}
        
function checkPassword() {
    var password = $('#Password');
    /*
    1) Should be 6-15 characters in length
    2) Should have atleast one lowercase character
    3) Should have atleast one uppercase character
    4) Should have atleast one number
    5) Should have atleast one special character
    6) Should not have spaces
    */
    password.keydown(function () {
        if (password.val().match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W)\S{6,15}$/)) {
            showUniqueInputSuccess(password)
        } else {
            showUniqueInputFail(password);
        }
    });
}

function checkPasswordConfirm() {
    var password = $('#Password');
    var repeatedPassword = $('#ConfirmPassword');
    repeatedPassword.keydown(function () {
        if(password.val() == repeatedPassword.val()){
            showUniqueInputSuccess(repeatedPassword);
        }
        else{
            showUniqueInputFail(repeatedPassword);
        }
    });
}

function checkEmailInput() {
    var email = $('#Email');

    email.keydown(function(){
        if(email.val().length >= 5){
            showInputLoader(email);
        }
        else{
            hideInputLoader(email);
            showUniqueInputFail(email);
        }
    });

    email.focusout(function () {
        if(IsEmail(email.val())){
            var url = "/register/checkEmail";
            var emailValue = email.val();
            $.ajax({
                async: true,
                url: url,
                data: { name: emailValue },
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    if (data["status"] == "ok") {
                        showUniqueInputSuccess(email);
                    }
                    else {
                        showUniqueInputFail(email);
                    } 
                    hideInputLoader(email);
                },
                erorr: function() {
                    hideInputLoader(email);
                }
            });
        }
        else {
            hideInputLoader(email);
            showUniqueInputFail(email);
        }
    });
}

function showInputLoader(inputTag) {
    var item = $(inputTag).closest('.form-group').find('.loader');
    hideUniqueInputStatus(item);
    item.show();
}

function hideInputLoader(inputTag) {
    $(inputTag).closest('.form-group').find('.loader').hide();
}

function hideUniqueInputStatus(inputTag) {
    $(inputTag).closest('.form-group').find('span').hide();
}

function showUniqueInputSuccess(inputTag) {
    $(inputTag).addClass("success-input");
    var item =  $(inputTag).closest('.form-group').find('span')
    if (item.hasClass('glyphicon-remove')) {
        item.removeClass('glyphicon-remove');
    }
    item.addClass('glyphicon-ok').show();
    displayRegisterButton();
}

function showUniqueInputFail(inputTag) {
    $(inputTag).removeClass("success-input");
    var item = $(inputTag).closest('.form-group').find('span')
    if (item.hasClass('glyphicon-ok')) {
        item.removeClass('glyphicon-ok');
    }
    item.addClass('glyphicon-remove').show();
    displayRegisterButton();
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isCorrectUsername(username) {
    var regex = /\S{6,15}$/;
    return regex.test(username);
}

function displayRegisterButton() {
    var successCount = $(".success-input").length;
    var inputCount = $(".form-control").length;
    if (successCount == inputCount) {
        $('#RegisterSubmit').prop('disabled', false);
    }
    else{
        
        $('#RegisterSubmit').prop('disabled', true);
    }
}