﻿/// <binding Clean='clean' />

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    project = require("./project.json"),
    rename = require('gulp-rename'),
    es = require('event-stream'),
    source = require('vinyl-source-stream'),
    browserify = require('browserify'),
    jshint = require('gulp-jshint');

var paths = {
    webroot: "./" + project.webroot + "/"
};

paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";

gulp.task('jshint', function () {

    var jshintConfig = {
        node: true
    };

    gulp
        .src('./Scripts/src/app/**/*.js')
        .pipe(jshint(jshintConfig))
        .pipe(jshint.reporter('jshint-stylish', { varbose: true }));

});

gulp.task('browserify', ['jshint'], function (bundleSrc, bundleName, bundleDir) {
    // we define our input files, which we want to have
    // bundled:
    var files = [
        './Scripts/src/dashboard/dashboard.js'
    ];

    // map them to our stream function
    var tasks = files.map(function (entry) {
        return browserify({ entries: [entry], debug: true })
            .bundle()
            .pipe(source(entry))
            // rename them to have "bundle as postfix"
            .pipe(rename({
                dirname: ''
            }))
            .pipe(gulp.dest(paths.webroot + "js"));
    });
    // create a merged stream
    return es.merge.apply(null, tasks);
});

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:css"]);

gulp.task("min:js", function () {
    gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", ["min:js", "min:css"]);
