﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using MTracker.DataEntities;
using MTracker.DataEntities.Models;
using MTracker.ServiceContracts;

namespace MTracker.Web.ApiControllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class DevicesController : Controller
    {
        private readonly IDeviceService _deviceService;

        public DevicesController(IDeviceService deviceService)
        {
            _deviceService = deviceService;
        }

        [HttpGet("{id}/allmeasurements")]
        public IEnumerable<DeviceMeasurementModel> GetAllMeasurements(string id)
        {
            return _deviceService.GetAllDeviceMeasurements(id);
        }

        [HttpGet("{id}/latestmeasurements")]
        public IEnumerable<DeviceMeasurementModel> GetLatestMeasurements(string id, [FromQuery] int count)
        {
            return _deviceService.GetLatestDeviceMeasurements(id, count);
        }

        [HttpPost("savemeasurement")]
        public void SaveMeasurement([FromBody] DeviceMeasurement measurement)
        {
            measurement.Time = DateTime.Now;
            _deviceService.SaveMeasurement(measurement);
        }

        [HttpPost("save")]
        public void SaveDevice([FromBody] Device device)
        {
            _deviceService.SaveDevice(device);
        }
    }
}
