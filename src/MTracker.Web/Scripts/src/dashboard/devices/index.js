﻿'use strict';

module.exports = require('angular')
    .module('dashboard.devices', [])
    .controller('addDeviceController', require('./addDeviceController.js'))
    .controller('getSoftwareController', require('./getSoftwareController.js'))
    .name;