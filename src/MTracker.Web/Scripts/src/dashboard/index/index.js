﻿'use strict';

module.exports = require('angular')
    .module('dashboard.index', [])
    .controller('indexController', require('./indexController.js'))
    .name;