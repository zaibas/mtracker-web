﻿'use strict';

var settingsConfig = function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('index');

    $stateProvider
        .state('profile', {
            url: '/profile',
            template: '<h1>Profile</h1>',
            controller: 'profileController as ctrl',
            data: {
                title: 'Profile'
            }
        })
        .state('index', {
            url: '/index',
            template: '<h1>Dashboard Index</h1>',
            controller: 'indexController as ctrl',
            data: {
                title: 'Dashboard'
            }
        })
        .state('addDevice', {
            url: '/addDevice',
            template: '<h1>Add New Device</h1>',
            controller: 'addDeviceController as ctrl',
            data: {
                title: 'Add New Device'
            }
        })
        .state('getSoftware', {
            url: '/getSoftware',
            template: '<h1>Get Device Software</h1>',
            controller: 'getSoftwareController as ctrl',
            data: {
                title: 'Get Device Software'
            }
        })
        .state('settings', {
            url: '/settings',
            template: '<h1>Settings</h1>',
            controller: 'settingsController as ctrl',
            data: {
                title: 'Settings'
            }
        });
};

module.exports = ['$stateProvider', '$urlRouterProvider', settingsConfig];