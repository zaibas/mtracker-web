﻿'use strict';

module.exports = require('angular')
    .module('dashboard.settings', [])
    .controller('settingsController', require('./settingsController.js'))
    .name;