﻿'use strict';

var controller = function ($scope, $rootScope) {
    $scope.title = '';
    $scope.loaded = false;

    $rootScope.$on('$stateChangeStart', function (event, toState) {
        $scope.title = toState.data.title;
        $scope.loaded = true;
    });
};

module.exports = ['$scope', '$rootScope', controller];