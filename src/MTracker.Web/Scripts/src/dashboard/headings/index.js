﻿'use strict';

module.exports = require('angular')
    .module('dashboard.headings', [])
    .controller('headingsController', require('./headingsController.js'))
    .name;