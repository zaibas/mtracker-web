﻿'use strict';

require('angular')
    .module('dashboard', [
        require('angular-ui-router'),
        require('./index'),
        require('./profile'),
        require('./devices'),
        require('./settings'),
        require('./headings')
    ])
    .config(require('./config/routesConfig.js'));