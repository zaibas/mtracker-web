﻿'use strict';

module.exports = require('angular')
    .module('dashboard.profile', [])
    .controller('profileController', require('./profileController.js'))
    .name;