﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MTracker.Web.Models.Authentication
{
    public class LoginData
    {
        [Required]
        public String name { get; set; }
        [Required]
        public String pass { get; set; }
    }
}
