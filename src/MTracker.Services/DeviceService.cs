﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTracker.DataContracts;
using MTracker.DataEntities;
using MTracker.DataEntities.Models;
using MTracker.ServiceContracts;

namespace MTracker.Services
{
    public class DeviceService: IDeviceService
    {
        private readonly IDeviceMeasurementRepo _measurementRepository;
        private readonly IDeviceRepo _deviceRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeviceService(IDeviceMeasurementRepo measurementRepository, IUnitOfWork unitOfWork, IDeviceRepo deviceRepository)
        {
            _measurementRepository = measurementRepository;
            _unitOfWork = unitOfWork;
            _deviceRepository = deviceRepository;
        }

        public IEnumerable<DeviceMeasurementModel> GetAllDeviceMeasurements(string deviceId)
        {
            return _measurementRepository
                .AsQueryable()
                .Where(x => x.DeviceId == deviceId)
                .Select(x => new DeviceMeasurementModel
                {
                    Measurement = x.Measurement,
                    Time = x.Time
                })
                .ToList();
        }

        public IEnumerable<DeviceMeasurementModel> GetLatestDeviceMeasurements(string deviceId, int count)
        {
            var query = _measurementRepository
                .AsQueryable()
                .Where(x => x.DeviceId == deviceId);

            return query
                .Skip(Math.Max(0, query.Count() - count))
                .Select(x => new DeviceMeasurementModel
                {
                    Measurement = x.Measurement,
                    Time = x.Time
                })
                .ToList();
        }

        public void SaveMeasurement(DeviceMeasurement measurement)
        {
            _measurementRepository.Add(measurement);
            _unitOfWork.Commit();
        }

        public void SaveDevice(Device device)
        {
            _deviceRepository.Add(device);
            _unitOfWork.Commit();
        }
    }
}