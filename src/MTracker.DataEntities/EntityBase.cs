﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MTracker.DataEntities
{
    public abstract class EntityBase: IEntity
    {
        public string Id { get; set; }
    }
}
