﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MTracker.DataEntities
{
    public class ApplicationUser: IdentityUser
    {
        public virtual ICollection<Device> Devices{ get; set; }
        public virtual string LastName { get; set; }
        public virtual string FirstName { get; set; }
    }
}
