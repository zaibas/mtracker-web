﻿using System;

namespace MTracker.DataEntities.Models
{
    public class DeviceMeasurementModel
    {
        public DateTime Time { get; set; }

        public double Measurement { get; set; } 
    }
}