﻿
namespace MTracker.DataEntities
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}
