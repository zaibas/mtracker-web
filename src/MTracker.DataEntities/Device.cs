﻿using System.Collections.Generic;

namespace MTracker.DataEntities
{
    public class Device : EntityBase
    {
        public virtual string Name { get; set; }
        public virtual string Location { get; set; }
        public virtual IList<DeviceMeasurement> Measurements { get; set; }
        public virtual string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}