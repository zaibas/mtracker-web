﻿using System;

namespace MTracker.DataEntities
{
    public class DeviceMeasurement : EntityBase
    {
        public virtual Device Device { get; set; }
        public virtual string DeviceId { get; set; }
        public virtual double Measurement { get; set; }
        public virtual DateTime Time { get; set; }
    }
}