﻿using System.Collections.Generic;
using System.Linq;

namespace MTracker.DataContracts
{
    public interface IGenericRepository<TEntity>
    {
        IEnumerable<TEntity> GetAll();
        IQueryable<TEntity> AsQueryable();
        TEntity GetById(string id);
        TEntity Add(TEntity entity);
        TEntity Delete(TEntity entity);
        void Edit(TEntity entity);
    }
}
