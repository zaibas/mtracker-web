﻿using MTracker.DataEntities;

namespace MTracker.DataContracts
{
    public interface IDeviceMeasurementRepo : IGenericRepository<DeviceMeasurement>
    {
         
    }
}