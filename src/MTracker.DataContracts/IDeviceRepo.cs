﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MTracker.DataEntities;

namespace MTracker.DataContracts
{
    public interface IDeviceRepo : IGenericRepository<Device>
    {
    }
}
