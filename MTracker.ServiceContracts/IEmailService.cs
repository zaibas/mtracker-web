﻿using System.Threading.Tasks;

namespace MTracker.ServiceContracts
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}