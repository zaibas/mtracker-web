﻿using System.Collections.Generic;
using MTracker.DataEntities;
using MTracker.DataEntities.Models;

namespace MTracker.ServiceContracts
{
    public interface IDeviceService
    {
        IEnumerable<DeviceMeasurementModel> GetAllDeviceMeasurements(string deviceId);

        IEnumerable<DeviceMeasurementModel> GetLatestDeviceMeasurements(string deviceId, int count); 

        void SaveMeasurement(DeviceMeasurement measurement);

        void SaveDevice(Device device);
    }
}